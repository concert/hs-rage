[![pipeline status](https://gitlab.com/concert/hs-rage/badges/master/pipeline.svg)](
    https://gitlab.com/concert/hs-rage/commits/master)

Haskell bindings for Rage

Requires meson for building the C components.

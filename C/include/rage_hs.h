#pragma once
#define RAGE_DISABLE_ANON_UNIONS 1
#include <loader.h>
#include <graph.h>
#include <jack_bindings.h>

// Wrappers for haskell runtime only accepting pointers
rage_LoadedElementKindLoadResult * rage_element_loader_load_hs(char * name);
rage_NewElementType * rage_element_kind_specialise_hs(
    rage_LoadedElementKind * t, rage_Atom ** params);

rage_NewJackBackend * rage_jack_backend_new_hs(
    uint32_t sample_rate, uint32_t buffer_size,
    rage_PortNames * inputs, rage_PortNames * outputs);

rage_NewGraph * rage_graph_new_hs(rage_BackendInterface * bi);
rage_Error * rage_graph_start_processing_hs(
    rage_Graph * g, rage_EventCb evt_cb, void * cb_ctx);
rage_NewGraphNode * rage_graph_add_node_hs(
    rage_Graph * g, rage_ElementType * cet,
    rage_TimeSeries const * ts);
rage_NewEventId * rage_graph_update_node_hs(
    rage_GraphNode * n, uint32_t const series_idx,
    rage_TimeSeries const * const ts);
rage_Error * rage_graph_transport_seek_hs(rage_Graph * g, rage_Time const * t);
rage_Error * rage_graph_connect_hs(
    rage_ConTrans * g,
    rage_GraphNode * source, uint32_t source_idx,
    rage_GraphNode * sink, uint32_t sink_idx);
rage_Error * rage_graph_disconnect_hs(
    rage_ConTrans * g,
    rage_GraphNode * source, uint32_t source_idx,
    rage_GraphNode * sink, uint32_t sink_idx);

// Wrappers for things that use anonymous unions
rage_InstanceSpec * rage_cet_get_spec_hs(rage_ElementType * cet);

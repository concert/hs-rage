#include "rage_hs.h"

#define RAGE_HS_STRUCT_WRAPPER(t, params, wrapped, wargs) \
    t * wrapped ## _hs params { \
        t * p = malloc(sizeof(t)); \
        *p = wrapped wargs; \
        return p; \
    }

RAGE_HS_STRUCT_WRAPPER(
    rage_LoadedElementKindLoadResult, (char * name),
    rage_element_loader_load, (name))

RAGE_HS_STRUCT_WRAPPER(
    rage_NewElementType, (rage_LoadedElementKind * k, rage_Atom ** params),
    rage_element_kind_specialise, (k, params))

RAGE_HS_STRUCT_WRAPPER(
    rage_NewGraph, (rage_BackendInterface * bi),
    rage_graph_new, (bi))

RAGE_HS_STRUCT_WRAPPER(
    rage_Error,
    (rage_Graph * g, rage_EventCb evt_cb, void * cb_ctx),
    rage_graph_start_processing, (g, evt_cb, cb_ctx))

RAGE_HS_STRUCT_WRAPPER(
    rage_NewGraphNode, (rage_Graph * g, rage_ElementType * cet, rage_TimeSeries const * ts),
    rage_graph_add_node, (g, cet, ts))

RAGE_HS_STRUCT_WRAPPER(
    rage_Error, (rage_Graph * g, rage_Time const * t),
    rage_graph_transport_seek, (g, t))

RAGE_HS_STRUCT_WRAPPER(
    rage_Error,
    (
        rage_ConTrans * g,
        rage_GraphNode * source, uint32_t source_idx,
        rage_GraphNode * sink, uint32_t sink_idx
    ),
    rage_graph_connect, (g, source, source_idx, sink, sink_idx))

RAGE_HS_STRUCT_WRAPPER(
    rage_Error,
    (
        rage_ConTrans * g,
        rage_GraphNode * source, uint32_t source_idx,
        rage_GraphNode * sink, uint32_t sink_idx
    ),
    rage_graph_disconnect, (g, source, source_idx, sink, sink_idx))

#undef RAGE_HS_STRUCT_WRAPPER

rage_InstanceSpec * rage_cet_get_spec_hs(rage_ElementType * cet) {
    return &cet->spec;
}

rage_NewEventId * rage_graph_update_node_hs(
        rage_GraphNode * n, uint32_t const series_idx,
        rage_TimeSeries const * const ts) {
    rage_NewEventId * rval = malloc(sizeof(rage_NewEventId));
    *rval = rage_graph_update_node(n, series_idx, *ts);
    return rval;
}

rage_NewJackBackend * rage_jack_backend_new_hs(
        uint32_t sample_rate, uint32_t buffer_size,
        rage_PortNames * inputs, rage_PortNames * outputs) {
    rage_NewJackBackend * rv = malloc(sizeof(rage_NewJackBackend));
    *rv = rage_jack_backend_new(sample_rate, buffer_size, *inputs, *outputs);
    return rv;
}

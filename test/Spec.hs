module Main where

import Control.Monad.Except (runExceptT)
import Data.List (isInfixOf)
import qualified Data.Map as Map
import Foreign.Marshal.Array (withArray)
import Foreign.Marshal.Utils (with)
import Foreign.Ptr (Ptr)
import System.Environment (getEnv)

import qualified Rage.Loader as L
import qualified Rage.Event as E
import qualified Rage.Graph as G
import qualified Rage.Connect as C
import qualified Rage.Jack as J
import Rage.Marshal.Atoms (withAtom)
import Rage.Types.Atoms (Atom(AInt, AFloat))
import Rage.Bindings.Chronology (Time(..))
import Rage.Types.Backend (SampleRate(..), PeriodSize(..))

inPorts, outPorts :: [String]
inPorts = ["in_L", "in_R"]
outPorts = ["out_L", "out_R"]

evtHandler :: E.Event -> IO ()
evtHandler = print

graphGo :: L.Type -> G.Graph p -> IO ()
graphGo t g = do
    putStrLn "Graph running"
    mn <- G.addNode t [Map.fromList [(Time 0 0, G.TimePoint [AFloat 1.0] G.InterpolationConst)]] g
    n <- mn
    tE <- runExceptT $ C.withTransaction g $ \ct -> do
        C.connect (Just n) 0 Nothing 0 ct
    putStrLn "Connected"
    G.setTransportState G.TransportRolling g
    either error pure tE

main :: IO ()
main = do
    l <- L.loader =<< getEnv "RAGE_ELEMENTS_PATH"
    availableKinds <- L.getKindList l
    putStrLn "Listed kinds"
    let ampKind = head $ filter (isInfixOf "libamp") availableKinds
    mK <- L.loadKind ampKind
    case mK of
        Nothing -> error "Kind load failed"
        Just k -> do
            putStrLn "Loaded kind"
            mT <- L.mkType k [[AInt 2]]
            putStrLn "Type loaded"
            case mT of
                Nothing -> error "Type specialise failed"
                Just t -> do
                    putStrLn "Type specialised"
                    mJB <- J.withJackBackend (SampleRate 44100) (PeriodSize 1024) inPorts outPorts $ \b -> G.withGraph
                        b
                        evtHandler
                        (graphGo t)
                    pure $ maybe (error "Backend create failed") (maybe (error "Graph ctx failed") id) mJB

import Control.Monad (unless)
import Data.Maybe (fromJust)
import Distribution.Simple
import Distribution.Simple.Setup
import Distribution.Simple.Utils (rawSystemExit)
import Distribution.Simple.LocalBuildInfo (LocalBuildInfo(localPkgDescr))
import Distribution.PackageDescription
  ( HookedBuildInfo, BuildInfo(..), PackageDescription(library), libBuildInfo
  , GenericPackageDescription)
import Distribution.Verbosity (Verbosity)
import System.Directory (doesDirectoryExist, getCurrentDirectory)
import System.FilePath (joinPath)

main = defaultMainWithHooks simpleUserHooks
  { confHook = confC
  , preBuild = preBuildC
  , cleanHook = cleanC
  }

confC
  :: (GenericPackageDescription, HookedBuildInfo) -> ConfigFlags
  -> IO LocalBuildInfo
confC (desc, buildInfo) flags = do
    mesonBuild configVerbosity configDistPref flags
    localBuildInfo <- confHook simpleUserHooks (desc, buildInfo) flags
    let origPackageDesc = localPkgDescr localBuildInfo
        origLib = fromJust $ library origPackageDesc
        origLibBuildInfo = libBuildInfo origLib
    curDir <- getCurrentDirectory
    rageIsSubproject <- doesDirectoryExist $ joinPath $
        mesonSourceDir : rageSubprojectSegs
    pure $ localBuildInfo {
        localPkgDescr = origPackageDesc {
            library = Just $ origLib {
                libBuildInfo = origLibBuildInfo {
                      extraLibDirs = mkAbs curDir <$>
                        getLibDirs rageIsSubproject
                    , includeDirs = mkAbs curDir <$>
                        getIncludeDirs rageIsSubproject
                }
            }
        }
      }
  where
    mkAbs curDir relP = joinPath [curDir, relP]
    rageSubprojectSegs = ["subprojects", "rage"]
    buildDir = mesonBuildDir configDistPref flags
    getLibDirs rageIsSubproject = let hsr = [buildDir] in
        if rageIsSubproject then (joinPath $ buildDir : rageSubprojectSegs) : hsr else hsr
    getIncludeDirs rageIsSubproject =
      let
        hsr = [joinPath [mesonSourceDir, "include"]]
        getSubInc s = joinPath
            [mesonSourceDir, "subprojects", "rage", s, "include"]
      in
        if rageIsSubproject
            then hsr ++ (getSubInc <$> ["graph", "types", "langext"])
            else hsr

preBuildC args flags = do
    mesonBuild buildVerbosity buildDistPref flags
    preBuild simpleUserHooks args flags

getShExec :: (a -> Flag Verbosity) -> a -> [String] -> IO ()
getShExec getVerbosity flags = rawSystemExit (fromFlag $ getVerbosity flags) "env"

mesonBuild :: (a -> Flag Verbosity) -> (a -> Flag FilePath) -> a -> IO ()
mesonBuild getVerbosity getDistP flags =
  let
    buildDir = mesonBuildDir getDistP flags
    shExec = getShExec getVerbosity flags
  in do
    buildDirExists <- doesDirectoryExist buildDir
    if buildDirExists
        then shExec ["meson", "subprojects", "update", "--sourcedir", mesonSourceDir]
        else shExec ["meson", buildDir, mesonSourceDir]
    shExec ["ninja", "-C", buildDir]

cleanC :: PackageDescription -> () -> UserHooks -> CleanFlags -> IO ()
cleanC packageDescription () hooks flags = do
    getShExec cleanVerbosity flags
        ["ninja", "-C", mesonBuildDir cleanDistPref flags, "clean"]
    (cleanHook simpleUserHooks) packageDescription () hooks flags

mesonBuildDir :: (a -> Flag FilePath) -> a -> FilePath
mesonBuildDir getDistPref flags = joinPath
    [fromFlag $ getDistPref flags, "rage-hs"]

mesonSourceDir :: FilePath
mesonSourceDir = "C"

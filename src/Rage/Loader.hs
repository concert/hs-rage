module Rage.Loader
  ( Loader, loader, KindInfo, getKindList, Bounds(..), EnumOpt(..)
  , Constraints(..), AtomDef(..), FieldDef(..), TupleDef(..)
  , Kind(paramDefs), loadKind, InstanceSpec(..), Type, mkType
  , typeInstanceSpec, typeRef, StreamDef(..), InterpolationLimit(..)
  , InterpolableTupleDef
  ) where

import Prelude hiding (fail)

import Control.Monad (void)
import Control.Monad.Fail (MonadFail(..))
import Data.IORef

import Foreign.Ptr (Ptr, plusPtr, nullPtr)
import Foreign.C.String (peekCString)
import Foreign.Storable (Storable(peek))
import Foreign.Marshal.Utils (with)
import Foreign.Marshal.Array (peekArray)

import Rage.Bindings.Macroesque (raLen)
import qualified Rage.Bindings.Loader as LB
import Rage.Bindings.Loader (StreamDef(..), InterpolationLimit(..))
import Rage.Marshal.Macroesque (raToList)
import Rage.Marshal.Atoms (withTuples, peekTuple)
import Rage.Types.Atoms (Tuple)
import Rage.Types.Defs
import Rage.Types.Time

newtype Loader = Loader (IORef LB.ElementLoader)

addFinaliser :: (a -> IO ()) -> a -> IO (IORef a)
addFinaliser fin a = do
    ior <- newIORef a
    void $ mkWeakIORef ior $ fin a
    pure ior

loader :: String -> IO Loader
loader s = do
    el <- LB.elementLoaderNew s
    Loader <$> addFinaliser LB.elementLoaderFree el

type KindInfo = String

getKindList :: Loader -> IO [KindInfo]
getKindList (Loader ior) =
    readIORef ior >>= LB.elementLoaderList >>= peek >>= raToList
    >>= mapM peekCString

loadParamDefs :: LB.LoadedElementKind -> IO [TupleDef]
loadParamDefs ek =
    LB.elementKindParameters ek >>= LB.peekParamDefList >>= raToList
    >>= mapM inflateTupleDef

inflateTupleDef :: LB.TupleDef -> IO TupleDef
inflateTupleDef (LB.TupleDef n desc def fields) = do
    fds <- raToList fields >>= mapM inflateFieldDef
    TupleDef
        <$> peekCString n
        <*> peekCString desc
        <*> (if def == nullPtr
            then pure Nothing
            else fmap Just $ peekTuple fds def)
        <*> pure fds
  where
    inflateFieldDef (LB.FieldDef n t) =
        FieldDef <$> peekCString n <*> peekAtomDef t
    peekAtomDef adp = peek adp >>= \(LB.AtomDef n c) ->
        AtomDef <$> peekCString n <*> inflateConstraints c
    inflateConstraints c = case c of
        LB.IConstraints mi ma -> pure $ ConInt $ Bounds
            (fromIntegral <$> mi) (fromIntegral <$> ma)
        LB.FConstraints mi ma -> pure $ ConFloat $ Bounds
            (realToFrac <$> mi) (realToFrac <$> ma)
        LB.TConstraints mi ma -> pure $ ConTime $ Bounds mi ma
        LB.SConstraints ms -> ConString <$> maybe (pure Nothing) (fmap Just . peekCString) ms
        LB.EConstraints opts -> ConEnum <$> (raToList opts >>= mapM peekEnumOpt)
    peekEnumOpt (LB.EnumOpt v n) = EnumOpt v <$> peekCString n

data Kind = Kind {kRef :: IORef LB.LoadedElementKind, paramDefs :: [TupleDef]}

instance Eq Kind where
    a == b = kRef a == kRef b

loadKind :: MonadFail m => KindInfo -> IO (m Kind)
loadKind ki =
    LB.elementLoaderLoad ki >>= LB.peekLoadedElementKindLoadResult
    >>= either (fmap fail . peekCString) (fmap pure . asKind)
  where
    asKind k = Kind <$> addFinaliser LB.elementLoaderUnload k <*> loadParamDefs k

type InterpolableTupleDef = (InterpolationLimit, TupleDef)
data InstanceSpec = InstanceSpec
  { isControls :: [InterpolableTupleDef]
  , isInputs :: [StreamDef]
  , isOutputs :: [StreamDef]
  }

data Type = Type
  { typeKind :: Kind
  , typeRef :: (IORef LB.ElementType)
  , typeInstanceSpec :: InstanceSpec
  }

instance Eq Type where
    a == b = typeRef a == typeRef b

inflateInterpolableTupleDef :: LB.TupleDef -> IO InterpolableTupleDef
inflateInterpolableTupleDef td = (,)
    -- FIXME: This is silly, we just read this from a pointer, and we've ended
    -- up having to allocate another copy to make this call:
    <$> with td LB.interpolationLimit
    <*> inflateTupleDef td

mkType :: MonadFail m => Kind -> [Tuple] -> IO (m Type)
mkType k params = do
    kptr <- readIORef $ kRef k
    cte <- withTuples params $ \p -> LB.elementTypeSpecialise kptr p >>= peek
    case cte of
        Left msg -> do
            fail <$> peekCString msg
        Right ct -> do
            fmap pure <$> Type k
                <$> addFinaliser LB.elementTypeFree ct
                <*> (LB.peekInstanceSpec ct >>= inflateInstanceSpec)
  where
    inflateInstanceSpec (LB.InstanceSpec c i o) = InstanceSpec
        <$> (raToList c >>= mapM inflateInterpolableTupleDef)
        <*> (fmap (toEnum . fromIntegral) <$> raToList i)
        <*> (fmap (toEnum . fromIntegral) <$> raToList o)

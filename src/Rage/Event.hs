module Rage.Event
  ( Event, EventId, eventFromPtr, eventType, eventId, eventMsg
  ) where

import Foreign.Ptr (nullPtr)
import Foreign.ForeignPtr (ForeignPtr, newForeignPtr, withForeignPtr)
import Foreign.C.String (peekCString)
import System.IO.Unsafe (unsafePerformIO)

import qualified Rage.Bindings.Event as E

type EventId = E.EventId

newtype Event = Event (ForeignPtr E.Event)

eventFromPtr :: E.EventPtr -> IO Event
eventFromPtr p = Event <$> newForeignPtr E.eventFree' p

newtype EventType = EventType E.EventType deriving Eq

eventType :: Event -> EventType
eventType (Event fp) = unsafePerformIO $ withForeignPtr fp $
    fmap EventType . E.eventType

instance Show EventType where
    show (EventType p) = unsafePerformIO $ do
        tyS <- peekCString p
        pure $ tyS ++ "<" ++ show p ++ ">"

eventId :: Event -> EventId
eventId (Event fp) = unsafePerformIO $ withForeignPtr fp E.eventId

eventMsg :: Event -> String
eventMsg (Event fp) = unsafePerformIO $ withForeignPtr fp $ \p -> do
    msgP <- E.eventMsg p
    if msgP == nullPtr
        then pure ""
        else peekCString msgP

instance Show Event where
    show e = show (eventType e) ++ " (" ++ show (eventId e) ++ ") " ++ eventMsg e

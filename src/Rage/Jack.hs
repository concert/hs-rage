module Rage.Jack (JackBackend, withJackBackend) where

import Prelude hiding (fail)
import Control.Exception (finally)
import Control.Monad.Fail (MonadFail(..))
import Foreign.C.String (peekCString, withCString)
import Foreign.Marshal.Utils (with)

import Rage.Types.Backend (SampleRate(..), PeriodSize(..), Backend(..))
import qualified Rage.Bindings.JackBackend as JB
import Rage.Marshal.Util (withWiths)
import Rage.Marshal.Macroesque (withRa)
import Rage.Bindings.Loader () -- Storable (RageArray CString)

data JackBackend = JackBackend JB.JackBackend JB.BackendInterface

withJackBackend
  :: MonadFail m
  => SampleRate -> PeriodSize
  -> [String] -> [String]
  -> (JackBackend -> IO a) -> IO (m a)
withJackBackend (SampleRate sampleRate) (PeriodSize periodSize) ins outs act = do
    newBeP <- withStrRa ins $ \ia -> withStrRa outs $ \oa ->
        JB.jackBackendNew sampleRate periodSize ia oa 
    newBe <- JB.peekNewJackBackend newBeP
    case newBe of
        Left msg -> fail <$> peekCString msg
        Right jbe -> pure <$> finally (go jbe) (JB.jackBackendFree jbe)
  where
    withStrRa hss act = withWiths withCString hss $ \css -> withRa css $ \ra -> with ra act
    go jbe = do
        i <- JB.jackBackendGetInterface jbe
        act $ JackBackend jbe i

instance Backend JackBackend where
    backendInterface (JackBackend _ i) = i

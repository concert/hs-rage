{-# LANGUAGE TypeFamilies #-}

module Rage.Graph
  ( Graph(gRef), withGraph, TimeSeries
  , TimePoint(..), InterpolationMode(..)
  , Node(nRef), addNode, updateNode, removeNode
  , TransportState(..), setTransportState, transportSeek
  ) where

import Prelude hiding (fail)
import Control.Concurrent.MVar
import Control.Monad.Fail (MonadFail(..))
import Control.Exception (finally)
import Data.IORef (readIORef)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map (Map)
import qualified Data.Map as Map
import Foreign.C.Types (CUInt)
import Foreign.C.String (peekCString, withCString)
import Foreign.Marshal.Array (withArray)
import Foreign.Marshal.Utils (with)
import Foreign.Ptr (Ptr, nullPtr, FunPtr, freeHaskellFunPtr)
import Foreign.Storable (Storable(peek))

import qualified Rage.Event as E
import qualified Rage.Bindings.Graph as G
import Rage.Bindings.Graph (TransportState(..), InterpolationMode(..))
import qualified Rage.Bindings.Atoms as A
import Rage.Bindings.Chronology (Time)
import Rage.Bindings.Macroesque (RageArray)
import Rage.Marshal.Macroesque (withRa)
import Rage.Marshal.Atoms (withTuple)
import Rage.Loader (Type(typeRef))
import Rage.Types.Atoms (Tuple)
import Rage.Types.Backend (Backend(..))
import Rage.Marshal.Util (withWiths)

data Graph p = Graph {gRef :: G.Graph, gFunPtr :: G.EventCb, gNodes :: MVar (Set (Node p))}

withGraph
  :: (Backend b, MonadFail m)
  => b -> (E.Event -> IO ()) -> (Graph p -> IO a) -> IO (m a)
withGraph b hsEvtHandler act = do
    ng <- G.graphNew (backendInterface b) >>= G.peekNewGraph
    case ng of
        Left msg -> fail <$> peekCString msg
        Right gp -> finally (start gp) (G.graphFree gp)
  where
    start gp = do
        evtHandler <- mkWrapped unwrappedEvtHandler
        err <- G.graphStartProcessing gp evtHandler nullPtr >>= peek
        case err of
            Left msg -> do
                freeHaskellFunPtr evtHandler
                fail <$> peekCString msg
            Right () -> do
              g <- Graph gp evtHandler <$> newMVar mempty
              pure <$> finally (act g) (stop g)
    stop (Graph r hfp mns) = do
        ns <- takeMVar mns
        mapM_ (G.graphRemoveNode r . nRef) ns
        G.graphStopProcessing r
        freeHaskellFunPtr hfp
    unwrappedEvtHandler _ p = E.eventFromPtr p >>= hsEvtHandler

type family WrapperType a where
    WrapperType (FunPtr a) = a -> IO (FunPtr a)

foreign import ccall "wrapper" mkWrapped :: WrapperType G.EventCb

setTransportState :: TransportState -> Graph p -> IO ()
setTransportState s g = G.graphSetTransportState (gRef g) s

transportSeek :: MonadFail m => Time -> Graph p -> IO (m ())
transportSeek t g =
    (with t $ G.graphTransportSeek $ gRef g) >>= peek
    >>= either (fmap fail . peekCString) (fmap pure . pure)

data Node p = Node {nGraph :: Graph p, nRef :: G.GraphNode}

instance Eq (Node p) where
    a == b = nRef a == nRef b

instance Ord (Node p) where
    compare a b = compare (nRef a) (nRef b)

data TimePoint = TimePoint Tuple InterpolationMode
type TimeSeries = Map Time TimePoint

withTimeSeries :: TimeSeries -> (G.TimeSeries -> IO a) -> IO a
withTimeSeries hsTs action = withWiths withTimePoint (Map.toList hsTs)
    $ \cTs -> withRa cTs action
  where
    withTimePoint (t, TimePoint tup m) act =
        withTuple tup $ \aptr -> act $ G.TimePoint t aptr m

addNode :: MonadFail m => Type -> [TimeSeries] -> Graph p -> IO (m (Node p))
addNode ty hsTss g = do
    cet <- readIORef $ typeRef ty
    ngnp <- withWiths withTimeSeries hsTss
        $ \tss -> withArray tss $ G.graphAddNode (gRef g) cet
    ngn <- G.peekNewGraphNode ngnp
    either (fmap fail . peekCString) (pure . pure . Node g) ngn

updateNode :: MonadFail m => Node p -> Int -> TimeSeries -> IO (m E.EventId)
updateNode n idx ts = do
    neidp <- withTimeSeries ts $ \gts ->
        with gts $ G.graphUpdateNode (nRef n) (fromIntegral idx)
    neid <- G.peekNewEventId neidp
    either (fmap fail . peekCString) (pure . pure) neid

removeNode :: Node p -> IO ()
removeNode n = do
    let nmv = gNodes $ nGraph n
    nodes <- takeMVar nmv
    putMVar nmv $ Set.delete n nodes
    G.graphRemoveNode (gRef $ nGraph n) $ nRef n

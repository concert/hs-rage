module Rage.Bulk (BulkBackend, withBulkBackend) where

import Control.Exception (bracket)

import qualified Rage.Bindings.BulkBackend as BB
import Rage.Bindings.Backend (BackendInterface)
import Rage.Types.Backend (SampleRate(..), PeriodSize(..), Backend(..))

data BulkBackend = BulkBackend {unBb :: BB.BulkBackend, bbI :: BackendInterface}

withBulkBackend :: SampleRate -> PeriodSize -> (BulkBackend -> IO a) -> IO a
withBulkBackend (SampleRate sampleRate) (PeriodSize periodSize) = bracket
    (do
        bbp <- BB.bulkBackendNew sampleRate periodSize
        BulkBackend bbp <$> BB.bulkBackendGetInterface bbp)
    (BB.bulkBackendFree . unBb)

instance Backend BulkBackend where
    backendInterface = bbI

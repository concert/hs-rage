module Rage.Connect
  ( Transaction, EIES, withTransaction, connect, disconnect
  ) where

import Control.Monad.Except
import Foreign.C.String (peekCString)
import Foreign.C.Types (CUInt)
import Foreign.Ptr (nullPtr)
import Foreign.Storable (peek)

import Rage.Graph (Graph(gRef), Node(nRef))
import Rage.Bindings.Graph
  ( ConTrans, graphConTransStart, graphConTransCommit, graphConTransAbort
  , graphConnect, graphDisconnect, worldNode, GraphNode, ErrorPtr)

data Transaction p0 p1 = Transaction ConTrans

type EIES = ExceptT String IO

withTransaction :: Graph p0 -> (Transaction p0 p1 -> EIES a) -> EIES a
withTransaction g act = do
    ct <- lift $ graphConTransStart $ gRef g
    go ct `catchError` abort ct
 where
    go ct = do
        r <- act $ Transaction ct
        lift $ graphConTransCommit ct
        pure r
    abort ct e = do
        lift $ graphConTransAbort ct
        throwError e

type CdOp = ConTrans -> GraphNode -> CUInt -> GraphNode -> CUInt -> IO ErrorPtr

cdOp :: CdOp -> Maybe (Node p0) -> Int -> Maybe (Node p0) -> Int -> Transaction p0 p1 -> EIES ()
cdOp cd mfn fi mtn ti (Transaction ct) = do
    ep <- lift $ cd ct (mNRef mfn) (fromIntegral fi) (mNRef mtn) (fromIntegral ti)
    e <- lift $ peek ep
    case e of
        Left msg -> do
            hsMsg <- lift $ peekCString msg
            throwError hsMsg
        Right x -> pure x
  where
    mNRef = maybe worldNode nRef

connect :: Maybe (Node p0) -> Int -> Maybe (Node p0) -> Int -> Transaction p0 p1 -> EIES ()
connect = cdOp graphConnect

disconnect :: Maybe (Node p0) -> Int -> Maybe (Node p0) -> Int -> Transaction p0 p1 -> EIES ()
disconnect = cdOp graphDisconnect

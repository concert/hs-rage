module Rage.Marshal.Atoms (withAtom, withTuple, withTuples, peekTuple) where

import Foreign.C.String (withCString, peekCString)
import Foreign.C.Types (CFloat(..))
import Foreign.Ptr (Ptr, plusPtr)
import Foreign.Marshal.Array (withArray)

import qualified Rage.Bindings.Atoms as A
import Rage.Marshal.Util (withWiths)
import Rage.Types.Defs (FieldDef(fdType), AtomDef(adConstraints), Constraints(..))
import Rage.Types.Atoms

withAtom :: Atom -> (A.Atom -> IO a) -> IO a
withAtom a act = case a of
    AInt i -> act $ A.AInt $ fromIntegral i
    AFloat f -> act $ A.AFloat $ CFloat f
    ATime t -> act $ A.ATime t
    AString s -> withCString s $ act . A.AString
    AEnum e -> act $ A.AEnum $ fromIntegral e

withTuples :: [Tuple] -> (Ptr (Ptr A.Atom) -> IO a) -> IO a
withTuples tups act = withWiths withTuple tups $ \cTups -> withArray cTups act

withTuple :: Tuple -> (Ptr A.Atom -> IO a) -> IO a
withTuple tup act = withWiths withAtom tup $ \cTup -> withArray cTup act

inflateAtom :: A.Atom -> IO Atom
inflateAtom a = case a of
    A.AInt i -> pure $ AInt $ fromIntegral i
    A.AFloat (CFloat f) -> pure $ AFloat f
    A.ATime t -> pure $ ATime t
    A.AString s -> AString <$> peekCString s
    A.AEnum e -> pure $ AEnum $ fromIntegral e

peekTuple :: [FieldDef] -> Ptr A.Atom -> IO [Atom]
peekTuple fieldDefs p = sequence $ zipWith
    (\def ptr -> A.peekAtom def ptr >>= inflateAtom)
    (toAtomType <$> fieldDefs)
    $ (plusPtr p . (* A.sizeOfAtom)) <$> [0..length fieldDefs]

toAtomType :: FieldDef -> A.AtomType
toAtomType fd = case adConstraints $ fdType fd of
    ConInt _ -> A.AtomInt
    ConFloat _ -> A.AtomFloat
    ConTime _ -> A.AtomTime
    ConString _ -> A.AtomString
    ConEnum _ -> A.AtomEnum

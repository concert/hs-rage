module Rage.Marshal.Macroesque where

import Foreign.Marshal.Array (peekArray, withArrayLen)
import Foreign.Storable (Storable)

import qualified Rage.Bindings.Macroesque as LM

raToList :: Storable a => LM.RageArray a -> IO [a]
raToList (LM.RageArray len items) = peekArray (fromIntegral len) items

withRa :: Storable a => [a] -> (LM.RageArray a -> IO b) -> IO b
withRa hsl act = withArrayLen hsl $ \l i -> act $ LM.RageArray (fromIntegral l) i

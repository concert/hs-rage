module Rage.Marshal.Util where

withWiths :: (a -> (b -> IO r) -> IO r) -> [a] -> ([b] -> IO r) -> IO r
withWiths w hs act = go [] hs
  where
    go acc [] = act $ reverse acc
    go acc (h:tail) = w h $ \c -> go (c:acc) tail

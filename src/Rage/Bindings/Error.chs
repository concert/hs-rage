{-# LANGUAGE FlexibleInstances #-}
#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Error where

import Foreign.Storable (Storable(..))

import Rage.Bindings.Macroesque

type Error = OrError ()

instance Storable Error where
    sizeOf _ = {# sizeof Error #}
    alignment _ = {# alignof Error #}
    peek p = peekEither {# get Error->half #} {# get Error->side.left #} (const $ pure ()) p
    poke p e = undefined -- Currently unused

{-# LANGUAGE StandaloneDeriving, GeneralizedNewtypeDeriving, FlexibleInstances #-}
#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Loader where

import Foreign.C.String (CString)
import Foreign.Storable (Storable(..))
import Foreign.Ptr (Ptr)
import Foreign.C.Types

import Rage.Bindings.Macroesque
import Rage.Bindings.Chronology
import Rage.Bindings.Atoms

{# pointer *ElementLoader newtype #}
{# fun element_loader_new as ^ {`String'} -> `ElementLoader' #}
{# fun element_loader_free as ^ {`ElementLoader'} -> `()' #}

{# pointer *ElementKinds -> RageArray CString #}
{# fun element_loader_list as ^ {`ElementLoader'} -> `ElementKinds' #}
{# fun element_kinds_free as ^ {`ElementKinds'} -> `()' #}

instance Storable (RageArray CString) where
    sizeOf _ = {# sizeof ElementKinds #}
    alignment _ = {# alignof ElementKinds #}
    peek = peekRageArray
        {# get ElementKinds->len #} {# get ElementKinds->items #}
    poke p (RageArray len items) = do
        {# set ElementKinds->len #} p len
        {# set ElementKinds->items #} p items

{# pointer *LoadedElementKind newtype #}
deriving instance Storable LoadedElementKind;
{# pointer *LoadedElementKindLoadResult -> OrError LoadedElementKind #}
{# fun element_loader_load_hs as elementLoaderLoad {`String'} -> `LoadedElementKindLoadResult' #}
{# fun element_loader_unload as ^ {`LoadedElementKind'} -> `()' #}

peekLoadedElementKindLoadResult :: LoadedElementKindLoadResult -> IO (OrError LoadedElementKind)
peekLoadedElementKindLoadResult = peekEither
    {# get LoadedElementKindLoadResult->half #}
    {# get LoadedElementKindLoadResult->side.left #}
    {# get LoadedElementKindLoadResult->side.right #}

data EnumOpt = EnumOpt {eoValue :: CInt, eoName :: CString}
{# pointer *EnumOpt as EnumOptPtr -> EnumOpt #}

instance Storable EnumOpt where
    sizeOf _ = {# sizeof EnumOpt #}
    alignment _ = {# alignof EnumOpt #}
    peek p = EnumOpt <$> {# get EnumOpt->value #} p <*> {# get EnumOpt->name #} p
    poke = undefined -- Not needed at the moment

data Constraints
  = IConstraints {cicMin :: Maybe CInt, cicMax :: Maybe CInt}
  | FConstraints {cfcMin :: Maybe CFloat, cfcMax :: Maybe CFloat}
  | TConstraints {ctcMin :: Maybe Time, ctcMax :: Maybe Time}
  | SConstraints (Maybe CString)
  | EConstraints (RageArray EnumOpt)

data AtomDef = AtomDef { adName :: CString, adConstraints :: Constraints}

instance Storable AtomDef where
    sizeOf _ = {# sizeof AtomDef #}
    alignment _ = {# alignof AtomDef #}
    peek p = do
        t <- toEnum . fromIntegral <$> {# get AtomDef->type #} p
        n <- {# get AtomDef->name #} p
        c <- case t of
            AtomInt -> IConstraints
                <$> peekMaybe {# get AtomDef->constraints.i.min.half #}
                    {#get AtomDef->constraints.i.min.side.right #} p
                <*> peekMaybe {# get AtomDef->constraints.i.max.half #}
                    {#get AtomDef->constraints.i.max.side.right #} p
            AtomFloat -> FConstraints
                <$> peekMaybe {# get AtomDef->constraints.f.min.half #}
                    {#get AtomDef->constraints.f.min.side.right #} p
                <*> peekMaybe {# get AtomDef->constraints.f.max.half #}
                    {#get AtomDef->constraints.f.max.side.right #} p
            AtomTime -> TConstraints
                <$> peekMaybe
                    {# get AtomDef->constraints.t.min.half #}
                    (const $ Time
                        <$> {#get AtomDef->constraints.t.min.side.right.second #} p
                        <*> {#get AtomDef->constraints.t.min.side.right.fraction #} p)
                    p
                <*> peekMaybe
                    {# get AtomDef->constraints.t.max.half #}
                    (const $ Time
                        <$> {#get AtomDef->constraints.t.max.side.right.second #} p
                        <*> {#get AtomDef->constraints.t.max.side.right.fraction #} p)
                    p
            AtomString -> SConstraints <$> peekMaybe
                {# get AtomDef->constraints.s.half #}
                {# get AtomDef->constraints.s.side.right #} p
            AtomEnum -> EConstraints <$> (RageArray
                <$> {# get AtomDef->constraints.e.len #} p
                <*> {# get AtomDef->constraints.e.items #} p)
        return $ AtomDef n c
    poke = undefined  -- We don't need this at the moment and it's long

{# pointer *AtomDef as AtomDefPtr -> AtomDef #}

data FieldDef = FieldDef {fdName :: CString, fdType :: AtomDefPtr}

instance Storable FieldDef where
    sizeOf _ = {# sizeof FieldDef #}
    alignment _ = {# alignof FieldDef #}
    peek p = FieldDef
        <$> {# get FieldDef->name #} p
        <*> {# get FieldDef->type #} p
    poke p (FieldDef n t) = do
        {# set FieldDef->name #} p n
        {# set FieldDef->type #} p t

{# pointer *Atom as AtomPtr -> Atom #}
{# pointer *FieldDef as FieldDefPtr -> FieldDef #}

data TupleDef = TupleDef
  { tdName :: CString, tdDescription :: CString, tdDefault :: AtomPtr
  , tdDefs :: RageArray FieldDef}

instance Storable TupleDef where
    sizeOf _ = {# sizeof TupleDef #}
    alignment _ = {# alignof TupleDef #}
    peek p = TupleDef
        <$> {# get TupleDef->name #} p
        <*> {# get TupleDef->description #} p
        <*> {# get TupleDef->default_value #} p
        <*> (RageArray <$> {# get TupleDef->defs.len #} p <*> {# get TupleDef->defs.items #} p)
    poke p (TupleDef name desc dval (RageArray len items)) = do
        {# set TupleDef->name #} p name
        {# set TupleDef->description #} p desc
        {# set TupleDef->default_value #} p dval
        {# set TupleDef->defs.len #} p len
        {# set TupleDef->defs.items #} p items

{# pointer *TupleDef as TupleDefPtr -> TupleDef #}
{# pointer *ParamDefList -> RageArray TupleDef #}
{# fun element_kind_parameters as ^ {`LoadedElementKind'} -> `ParamDefList' #}

peekParamDefList :: ParamDefList -> IO (RageArray TupleDef)
peekParamDefList = peekRageArray
    {# get ParamDefList->len #} {# get ParamDefList->items #}

{# pointer *ElementType newtype #}
deriving instance Storable ElementType
{# pointer *NewElementType -> OrError ElementType #}

instance Storable (OrError ElementType) where
    sizeOf _ = {# sizeof NewElementType #}
    alignment _ = {# alignof NewElementType #}
    peek = peekEither
        {# get NewElementType->half #}
        {# get NewElementType->side.left #}
        {# get NewElementType->side.right #}
    poke = undefined -- Unused at the moment

{# fun element_kind_specialise_hs as elementTypeSpecialise {`LoadedElementKind', id `Ptr AtomPtr'} -> `NewElementType' #}
{# fun element_type_free as ^ {`ElementType'} -> `()' #}

{# enum StreamDef {underscoreToCase} deriving (Eq, Show, Bounded) #}

data InstanceSpec = InstanceSpec
  { isControls :: RageArray TupleDef
  , isInputs :: RageArray CInt -- StreamDef
  , isOutputs :: RageArray CInt -- StreamDef
  }

{# pointer *InstanceSpec as InstanceSpecPtr -> InstanceSpec #}

peekInstanceSpec :: ElementType -> IO InstanceSpec
peekInstanceSpec (ElementType p) = InstanceSpec
    <$> peekRageArray
        {# get ElementType->spec.controls.len #}
        {# get ElementType->spec.controls.items #}
        p
    <*> peekRageArray
        {# get ElementType->spec.inputs.len #}
        {# get ElementType->spec.inputs.items #}
        p
    <*> peekRageArray
        {# get ElementType->spec.outputs.len #}
        {# get ElementType->spec.outputs.items #}
        p

{# enum InterpolationMode as InterpolationLimit {underscoreToCase} deriving (Eq, Ord, Show, Bounded) #}
{# fun interpolation_limit as ^ {`TupleDefPtr'} -> `InterpolationLimit' #}

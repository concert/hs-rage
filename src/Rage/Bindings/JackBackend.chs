{-# LANGUAGE GeneralizedNewtypeDeriving, StandaloneDeriving #-}
#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.JackBackend where

import Foreign.Storable (Storable)
import Foreign.C.String (CString)
import Foreign.C.Types (CUInt)

import Rage.Bindings.Macroesque (RageArray, OrError, peekEither)
import Rage.Bindings.Backend (BackendInterface')

{# pointer *PortNames -> RageArray CString #}

{# pointer *JackBackend newtype #}
deriving instance Storable JackBackend
{# pointer *NewJackBackend -> OrError JackBackend #}

{# fun jack_backend_new_hs as jackBackendNew
    {`CUInt', `CUInt', `PortNames', `PortNames'} -> `NewJackBackend' #}
{# fun jack_backend_free as ^ {`JackBackend'} -> `()' #}

peekNewJackBackend :: NewJackBackend -> IO (OrError JackBackend)
peekNewJackBackend = peekEither
    {# get NewJackBackend->half #}
    {# get NewJackBackend->side.left #}
    {# get NewJackBackend->side.right #}

{# pointer *BackendInterface -> BackendInterface' #}
{# fun jack_backend_get_interface as ^ {`JackBackend'} -> `BackendInterface' #}

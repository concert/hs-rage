#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Chronology where

import Foreign.C.Types (CULong, CUInt)
import Foreign.Storable (Storable(..))

data Time = Time CULong CUInt deriving (Eq, Ord, Show)

instance Storable Time where
    sizeOf _ = {# sizeof Time #}
    alignment _ = {# alignof Time #}
    peek p = Time <$> {# get Time->second #} p <*> {# get Time->fraction #} p
    poke p (Time s f) =
        {# set Time->second #} p s >> {# set Time->fraction #} p f

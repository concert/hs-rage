module Rage.Bindings.Backend where

import Foreign.Ptr (Ptr)

data BackendState'
type BackendState = Ptr BackendState'

data BackendInterface'
type BackendInterface = Ptr BackendInterface'

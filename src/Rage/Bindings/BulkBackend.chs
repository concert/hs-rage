{-# LANGUAGE GeneralizedNewtypeDeriving, StandaloneDeriving #-}
#include <rage_hs.h>
#include <bulk_backend.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.BulkBackend where

import Foreign.C.Types (CUInt)
import Foreign.Storable (Storable)

import Rage.Bindings.Backend (BackendInterface')

{# pointer *BulkBackend newtype #}
deriving instance Storable BulkBackend

{# fun bulk_backend_new as ^ {`CUInt', `CUInt'} -> `BulkBackend' #}
{# fun bulk_backend_free as ^ {`BulkBackend'} -> `()' #}

{# pointer *BackendInterface -> BackendInterface' #}
{# fun bulk_backend_get_interface as ^ {`BulkBackend'} -> `BackendInterface' #}

{-# LANGUAGE
    StandaloneDeriving, GeneralizedNewtypeDeriving, TypeSynonymInstances,
    FlexibleInstances #-}
#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Graph where

import Foreign.C.String (CString)
import Foreign.C.Types
import Foreign.Storable (Storable)
import Foreign.Ptr (Ptr, nullPtr)

import Rage.Bindings.Chronology
import Rage.Bindings.Macroesque
import Rage.Bindings.Error
import qualified Rage.Bindings.Loader as L
import Rage.Bindings.Atoms (Atom)
import Rage.Bindings.Event (Event, EventType, EventId)
import Rage.Bindings.Backend (BackendInterface')

{# pointer *BackendInterface -> BackendInterface' #}

{# pointer *Graph newtype #}
deriving instance Storable Graph
{# pointer *NewGraph -> OrError Graph #}

{# fun graph_new_hs as graphNew
    {`BackendInterface'} -> `NewGraph' #}
{# fun graph_free as ^ {`Graph'} -> `()' #}

peekNewGraph :: NewGraph -> IO (OrError Graph)
peekNewGraph = peekEither
    {# get NewGraph->half #}
    {# get NewGraph->side.left #}
    {# get NewGraph->side.right #}

{# pointer *Error as ErrorPtr -> Error #}
{# pointer *Event as EventPtr -> Event #}

type EventCb = {# type EventCb #}

-- c2hs doesn't let you generate this for a constant:
foreign import ccall "rage_hs.h rage_EventGraphStopped" stoppedEvent :: EventType

{# fun graph_start_processing_hs as graphStartProcessing
    {`Graph', id `EventCb', `Ptr ()'} -> `ErrorPtr' #}
{# fun graph_stop_processing as ^ {`Graph'} -> `()' #}

{# enum TransportState {underscoreToCase} deriving (Eq, Show, Bounded) #}

{# pointer *Time as TimePtr -> Time #}

{# fun graph_set_transport_state as ^ {`Graph', `TransportState'} -> `()' #}
{# fun graph_transport_seek_hs as graphTransportSeek {`Graph', `TimePtr'} -> `ErrorPtr' #}

{# pointer *GraphNode newtype #}
deriving instance Eq GraphNode
deriving instance Ord GraphNode
deriving instance Storable GraphNode
{# pointer *NewGraphNode -> OrError GraphNode #}

peekNewGraphNode :: NewGraphNode -> IO (OrError GraphNode)
peekNewGraphNode = peekEither
    {# get NewGraphNode->half #}
    {# get NewGraphNode->side.left #}
    {# get NewGraphNode->side.right #}

{# enum InterpolationMode {underscoreToCase} deriving (Eq, Show, Bounded) #}

{# pointer *Atom as AtomPtr -> Atom #}

data TimePoint = TimePoint
  { tpTime :: Time
  , tpValue :: AtomPtr
  , tpMode :: InterpolationMode}

{# pointer *TimePoint as TimePointPtr -> TimePoint #}

instance Storable TimePoint where
    sizeOf _ = {# sizeof TimePoint #}
    alignment _ = {# alignof TimePoint #}
    peek p = TimePoint
        <$> (Time
            <$> {#get TimePoint->time.second #} p
            <*> {#get TimePoint->time.fraction #} p)
        <*> {# get TimePoint->value #} p
        <*> (toEnum . fromIntegral <$> {#get TimePoint->mode #} p)
    poke p (TimePoint (Time s f) v m) = do
        {# set TimePoint->time.second #} p s
        {# set TimePoint->time.fraction #} p f
        {# set TimePoint->value #} p v
        {# set TimePoint->mode #} p $ fromIntegral $ fromEnum m

type TimeSeries = RageArray TimePoint
{# pointer *TimeSeries as TimeSeriesPtr -> TimeSeries #}

instance Storable TimeSeries where
    sizeOf _ = {# sizeof TimeSeries #}
    alignment _ = {# alignof TimeSeries #}
    peek = peekRageArray {# get TimeSeries->len #} {# get TimeSeries->items #}
    poke = pokeRageArray {# set TimeSeries->len #} {# set TimeSeries->items #}

-- This block is to get around C2Hs not maintaining mapping state between
-- files:
{# pointer *ElementType -> `L.ElementType' #}
unNewTypeCet :: L.ElementType -> Ptr L.ElementType
unNewTypeCet (L.ElementType p) = p

{# fun graph_add_node_hs as graphAddNode
    {`Graph', unNewTypeCet `L.ElementType', `TimeSeriesPtr'}
    -> `NewGraphNode' #}

{# pointer *NewEventId -> OrError EventId #}

{# fun graph_update_node_hs as graphUpdateNode {`GraphNode', `CUInt', `TimeSeriesPtr'} -> `NewEventId' #}

peekNewEventId :: NewEventId -> IO (OrError EventId)
peekNewEventId = peekEither
    {# get NewEventId->half #}
    {# get NewEventId->side.left #}
    {# get NewEventId->side.right #}

{# fun graph_remove_node as ^ {`Graph', `GraphNode'} -> `()' #}

{# pointer *ConTrans newtype #}
{# fun graph_con_trans_start as ^ {`Graph'} -> `ConTrans' #}
{# fun graph_con_trans_commit as ^ {`ConTrans'} -> `()' #}
{# fun graph_con_trans_abort as ^ {`ConTrans'} -> `()' #}
{# fun graph_connect_hs as graphConnect
    {`ConTrans', `GraphNode', `CUInt', `GraphNode', `CUInt'} -> `ErrorPtr' #}
{# fun graph_disconnect_hs as graphDisconnect
    {`ConTrans', `GraphNode', `CUInt', `GraphNode', `CUInt'} -> `ErrorPtr' #}

worldNode :: GraphNode
worldNode = GraphNode nullPtr

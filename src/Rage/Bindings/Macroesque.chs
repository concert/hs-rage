{-# LANGUAGE ScopedTypeVariables #-}
#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Macroesque where

import Foreign.Ptr (Ptr, nullPtr)
import Foreign.C.Types
import Foreign.C.String (CString)

data RageArray a = RageArray {raLen :: CUInt, raItems :: Ptr a}

peekRageArray :: (Ptr x -> IO CUInt) -> (Ptr x -> IO (Ptr a)) -> Ptr x -> IO (RageArray a)
peekRageArray getLen getItems p = RageArray <$> getLen p <*> getItems p

pokeRageArray
  :: (Ptr (RageArray a) -> CUInt -> IO ())
  -> (Ptr (RageArray a) -> Ptr a -> IO ())
  -> Ptr (RageArray a) -> RageArray a -> IO ()
pokeRageArray setLen setItems p (RageArray l i) = setLen p l >> setItems p i

{# enum EitherHalf {underscoreToCase} #}

peekEither
  :: (Ptr x -> IO CInt) -> (Ptr x -> IO a) -> (Ptr x -> IO b) -> Ptr x
  -> IO (Either a b)
peekEither getHalf getA getB p = do
    h <- getHalf p
    case toEnum $ fromIntegral h of
        EitherLeft -> Left <$> getA p
        EitherRight -> Right <$> getB p

type OrError a = Either CString a

peekMaybe :: forall x a. (Ptr x -> IO CInt) -> (Ptr x -> IO a) -> Ptr x -> IO (Maybe a)
peekMaybe halfGet rightGet p = either (const Nothing :: Ptr () -> Maybe a) Just
    <$> peekEither halfGet (const $ pure nullPtr) rightGet p

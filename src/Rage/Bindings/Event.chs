#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Event where

import Foreign.Ptr (Ptr, FunPtr)
import Foreign.C.String (CString)

type EventType' = {# type EventType #}
{# pointer *EventType -> EventType' #}

type EventId = {# type EventId #}

data Event
{# pointer *Event as EventPtr -> Event #}

-- Done this way so it can be used as a finaliser:
foreign import ccall "rage_hs.h &rage_event_free" eventFree' :: FunPtr (EventPtr -> IO ())

-- FIXME: Should I use pure on some/all of these?
{# fun event_type as ^ {`EventPtr'} -> `EventType' #}
{# fun event_msg as ^ {`EventPtr'} -> `CString' #}
{# fun event_id as ^ {`EventPtr'} -> `EventId' id #}

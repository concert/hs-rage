#include <rage_hs.h>

{# context lib="librage_hs" prefix="rage" #}

module Rage.Bindings.Atoms (Atom(..), AtomType(..), peekAtom, pokeAtom, sizeOfAtom) where

import Foreign.C.String (CString)
import Foreign.C.Types
import Foreign.Ptr (Ptr)
import Foreign.Storable (Storable(..))

import Rage.Bindings.Chronology (Time(..))

data Atom
  = AInt CInt
  | AFloat CFloat
  | ATime Time
  | AString CString
  | AEnum CInt

{# enum AtomType {underscoreToCase} #}

peekAtom :: AtomType -> Ptr Atom -> IO Atom
peekAtom t p = case t of
    AtomInt -> AInt <$> {# get Atom->i #} p
    AtomFloat -> AFloat <$> {# get Atom->f #} p
    AtomTime -> fmap ATime $ Time
        <$> {# get Atom->t.second #} p <*> {# get Atom->t.fraction #} p
    AtomString -> AString <$> {# get Atom->s #} p
    AtomEnum -> AEnum <$> {# get Atom->e #} p

pokeAtom :: Ptr Atom -> Atom -> IO ()
pokeAtom p a = case a of
    AInt i -> {# set Atom->i #} p i
    AFloat f -> {# set Atom->f #} p f
    ATime (Time s f) ->
        {# set Atom->t.second #} p s >> {# set Atom->t.fraction #} p f
    AString s -> {# set Atom->s #} p s
    AEnum e -> {# set Atom->e #} p e

sizeOfAtom :: Int
sizeOfAtom = {# sizeof Atom #}

alignOfAtom :: Int
alignOfAtom = {# alignof Atom #}

-- FIXME: This isn't storable, it's Pokeable (which isn't a thing) so I've made
-- this so I can use existing functions:
instance Storable Atom where
    sizeOf _ = sizeOfAtom
    alignment _ = alignOfAtom
    peek = undefined  -- Can't define this, doesn't make sense
    poke = pokeAtom

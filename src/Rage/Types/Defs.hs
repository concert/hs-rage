module Rage.Types.Defs where

import Foreign.C.Types (CInt)

import Rage.Types.Atoms (Tuple)
import Rage.Types.Time

data Bounds a = Bounds {min :: Maybe a, max :: Maybe a} deriving (Eq)

data EnumOpt = EnumOpt {eoValue :: CInt, eoName :: String}

data Constraints
  = ConInt (Bounds Int)
  | ConFloat (Bounds Float)
  | ConTime (Bounds Time)
  | ConString (Maybe String)
  | ConEnum [EnumOpt]

data AtomDef = AtomDef
  { adName :: String
  , adConstraints :: Constraints}

data FieldDef = FieldDef
  { fdName :: String
  , fdType :: AtomDef}

data TupleDef = TupleDef
  { tdName :: String, tdDescription :: String, tdDefault :: Maybe Tuple
  , tdDefs :: [FieldDef]}

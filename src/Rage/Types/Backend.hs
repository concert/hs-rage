module Rage.Types.Backend where

import Foreign.C.Types (CUInt)

import Rage.Bindings.Backend (BackendInterface)

newtype SampleRate = SampleRate CUInt
newtype PeriodSize = PeriodSize CUInt

class Backend a where
    backendInterface :: a -> BackendInterface

module Rage.Types.Atoms where

import Rage.Types.Time

data Atom
  = AInt Int
  | AFloat Float
  | ATime Time
  | AString String
  | AEnum Int
  deriving (Eq, Show)

type Tuple = [Atom]
